//a
'My name is: '


//b
'Ruchi'


//c
'My name is: ' + 'Ruchi'


//d
'Total cost: $' + (5 + 3)


//e
`Total cost: $${5 + 3}`


//f
alert(`Total cost: $${5 + 3}`);

//g
'Total cost: $' + (599 + 295) / 100


//h
`Total cost: $${(599 + 295) / 100}`

//i
alert(`Total cost: $${(599 + 295) / 100}`);

//j
alert(`Total cost: $${(599 + 295) / 100}
Thank you, come again!`);

//k
`Items (${2 + 2}): $${(2 * 2095 + 2 * 799) / 100}`

//l
`Shipping & handling: $${(499 + 499) / 100}`

//m
`Total before tax: $${(2 * 2095 + 2 * 799 + 499 + 499) / 100}`

//n
`Estimated tax (10%): $${Math.round((2 * 2095 + 2 * 799 + 499 + 499) * 0.1) / 100}`