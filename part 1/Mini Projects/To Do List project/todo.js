const todoList= [
    {name:'clean', dueDate:'2023-03-23'},
    {name:'mop',dueDate:'2013-12-22'}
];

renderTodoList();

function renderTodoList(){
    let todoListHtml='';
    for(let i=0;i<todoList.length;i++){
        const todoObject = todoList[i];
        // const name= todoObject.name;
        // const dueDate = todoObject.dueDate;
        const {name,dueDate} = todoObject;
        const html = `<p>
         <div> ${name} </div>
         <div> ${dueDate}  </div></p>
         <button onclick="
            todoList.splice(${i},1);
            renderTodoList();
         " class="delete-todo-button">Delete</button> 
       
         `;
        todoListHtml += html;
    }
    //console.log(todoListHtml);
    document.querySelector('.js-todo-list')
    .innerHTML = todoListHtml;
}

function addToDo(){

    const inputElement = document.querySelector('.js-name-input');
    const name = inputElement.value;
    
    const dateInputElement = document.querySelector('.js-due-date-input');
    const dueDate = dateInputElement.value;

    todoList.push({
        // name:name,
        // dueDate:dueDate
        //if both are same write directly
        name,
        dueDate
    }); 
    
    //console.log(todoList)

    
    // after adding or saving the value in field should get empty for that
   inputElement.value=``;
    //returns array

   renderTodoList();


   
}