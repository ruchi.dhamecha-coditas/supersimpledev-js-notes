const myArray = [10,20,30,[1,2],true,"hello",{name:"ruchi, age:21"}];

console.log(myArray);

console.log(myArray[1]);    //20

myArray[0] = 99;
console.log(myArray[0]);  //99

console.log(Array.isArray(myArray));  //true


myArray.splice(0,1);  //index we want to remvoe, no. of values to remove
console.log(myArray);
